#!/usr/bin/env python

import sys
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# init list with pin numbers on Pi3
# pin GPIO 7 is really pin 26
# pin GPIO 8 is really pin 24
relay = 7
switch = 8
GPIO.setup(relay,  GPIO.OUT)
GPIO.output(relay, GPIO.HIGH)
GPIO.setup(switch, GPIO.IN)

def usage():
    print("""
Usage: python door.py [command]

Options:
  command can be:
          STatus - show status of door (this is the default)
          OPen   - open the garage door
          CLose  - close the garage door
          BUtton|PUsh|PRess - press the garage door close button
    """)

def main():
  try:
    subcommand = sys.argv[1]
    if subcommand.startswith('st'):
      rc = status()
    elif subcommand.startswith('op'):
      rc = open()
    elif subcommand.startswith('cl'):
      rc = close()
    elif subcommand.startswith('bu') \
    or subcommand.startswith('pu') \
    or subcommand.startswith('pr'):
      rc = pressButton()
    else:
      print("Unknown subcommand: %s" % subcommand)
      usage()
      rc = -1
  except:
    rc = status()

  # cleanup and exit
  GPIO.cleanup()
  sys.exit(rc)

def status():
  if isOpen():
    print("open")
    return 0
  else:
    print("closed")
    return 1

#
# this will toggle the given pin for the given numer of seconds
#
def pressButton():
  secs = 0.25
  try:
    GPIO.output(relay, GPIO.LOW)
    time.sleep(secs)
    GPIO.output(relay, GPIO.HIGH)
    return 0
  except:
    print("Issues in pressButton")
    return -1

#
# the switch is normally open, so if we get
# input, it is closed.
#
def isClosed():
  if (GPIO.input(switch)):
    return True
  else:
    return False

def isOpen():
  return not isClosed()

#
# close door if open
#
# We will wait for 30 seconds for door to close. Either by detecting
# by the switch, or by timeout after 6 tries.
#
def close():
  if (isOpen()):
    print("Closing garage door")
    pressButton()
    tries = 6
    while isOpen():
      tries -= 1
      if not tries:
         break
      time.sleep(5)
  else:
    print("Garage door is already closed")
  return 0

#
# open door if closed
#
# We will wait 30 seconds before doing anything else
#
#
def open():
  if (isClosed()):
    print("Opening garage door")
    pressButton()
    time.sleep(30)
  else:
    print("Garage door is already open")
  return 0

main()

