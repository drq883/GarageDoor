<?php 
  # returns either open or closed
  $switch = exec('/usr/bin/sudo /usr/local/bin/door');

  if(isset($_GET['trigger']) && $_GET['trigger'] == 1) {
    error_reporting(E_ALL);
    exec('/usr/bin/sudo /usr/local/bin/door button');
    # an accidental page refresh causes the garage door to open
    # the following two lines must be before any HTML to prevent 
    # accidental door openings
    header('Location: /index.php');
    die();
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>GaragePi Opener</title>
    <!--

I saved index.php to my Home screen on the iPhone. Then when I clicked it, it showed
up in full-screen. When I clicked the raspberry logo to close the door, it would relaunch
into safari. Just kinda dumb, I thought. By removing the meta tag below, it launches directly
in safari. Problem solved.

    <meta name="apple-mobile-web-app-capable" content="yes">    

    -->
  </head>
  <body>
    <table align="center">
      <tr>
        <th colspan="4"><a id="GaragePi Door Opener" href="/index.php"><font size="8">GaragePi Door Opener</font></a></th>
      </tr>
    </table>
    <table align="center">
      <tr>
        <td align="center">
          <svg height="100" width="100">
            <circle cx="50" cy="50" r="40" stroke="white" stroke-width="3" fill="
<?php
if ($switch == "closed") {
  echo "red";
} else {
  echo "limegreen";
}
?>
            "></circle>
          </svg>
        </td>
      </tr>
    </table>
    <table align="center">
      <tr>
        <th colspan="4"><a id="STATUS" href="/index.php">
          <font size="8">
<?php
if ($switch == "closed") {
   echo "Closed";
} else {
   echo "Open";
}
?>
        </font></a></th>
      </tr>
    </table>
    <br><br>
    <table align="center" border=0>
      <tr>
        <td colspan="4" align="center"><a href='/index.php?trigger=1'><img src="remote-background.jpg" alt="garage remote" style="width:100%"></a></td>;
      </tr>
    </table>
  </body>
</html>
