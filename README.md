## GarageDoor

I had always wanted a way to verify that my garage door was closed. You know the feeling you get when you can't remember closing it right after you get out of your neighborhood on the way to work. Well I had an extra Pi laying around and decided to try and make it work.

### Materials

The first step was to get the materials I needed. I found several pages that detailed how to make a garage door controller, The parts I ordered from Amazon (I already had the Pi) were:
* [2 Port Relay](http://www.amazon.com/dp/B00P7Q88SS?psc=1) to "push" the remote button
* [Jumper wires](http://www.amazon.com/40pcs-Female-2-54mm-Jumper-2x40pcs/dp/B00GSE2S98/ref=pd_sim_147_6?ie=UTF8&dpID=51V4T8HPsEL&dpSrc=sims&preST=_AC_UL160_SR160%2C160_&refRID=1FASQPPFWNGKY4E2JAXC) to connect the Pi GPIO pins to the Relay
* [Door switch](http://www.amazon.com/gp/product/B0009SUF08/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B0009SUF08&linkCode=as2&tag=driscocityc0a-20&linkId=Y3OFNKEOINL6LPKT) to connect to the Pi to tell when door is open or closed
* 10K resister (part of my [Kuman bread board kit](https://www.amazon.com/gp/product/B01LYN4J3B/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1))

### Preparing the Pi

This was pretty simple:
* Install raspbian
* sudo apt update
* sudo apt upgrade
* sudo apt install git apache2 php
* git clone https://gitlab.com/drq883/GarageDoor.git

### Attaching the relay and switch

#### Identifying the pins on the Pi

If you orient the board with the 40 pins on the right, the pins are numbered sequentially left-to-right, top-to-bottom as in
```
(1) (2)
(3) (4)
(5) (6)
...
```

##### Relay
The relay has 4 pins. Connect 3 of them to the pins on the Pi 3B as follows:
```
pin4  (5.0) ----------------- VOC
pin26 (GPIO 7) -------------- IN1
pin6  (GND) ----------------- GND
```

Beside the number 1 relay, there are 3 screw connections. You need to hook up the top screw (relay 1) and the middle screw to the same place on your garage door opener where the wall mounted door opener wires are attached.

##### Switch
The switch has a normally open screw and a common screw. We will use these two so if there is no circuit through the switch, we will assume the door is open. You'll have to mount the switch somewhere on the door so the two pieces will be next to each other (1/4" in testing is about the max distance apart). And run two wires to the place where the Pi will be (probably on top of the opener).

Connect the two wires to the Pi as follows:
```
pin1 (3.3) ----------------- ---+
                                |
                                +
                                  /
                                 /
                                +
                                |
pin24 (GPIO 8) -----------------+
                                |
pin25 (GND) -- <10K resister> --+
```

### Source code

There are only 4 files in this repo:

* README.md - what you're reading
* door.sudoers - gives permission to www-data to run door
* door.py   - Python script to operate relay and detect switch
* index.php - PHP script to show status of door, and activate opener

### Installation

* `sudo mkdir -p /usr/local/bin`
* `sudo install door.py /usr/local/bin/door`
* `sudo cp index.php /var/www/html/index.php`
* `sudo cp door.sudoers /etc/sudoers.d/door`

Update apache config directive DirectoryIndex to look for index.php first and restart apache2.

Find a JPEG you like as the icon for the page and copy it to /var/www/html as remote-background.jpg.

That should be it.

The URL for the code is http://<your-pi-ip>

** References **
1. http://www.wellsbs.xyz/garagepi.html (currently dead)
1. https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/turing-machine/two.html
1. https://www.instructables.com/id/Garage-Door-Opener-2/
